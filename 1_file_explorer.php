<?php

$files = [];

foreach (new DirectoryIterator(getcwd()) as $file) {
    if ($file->isFile()) {
        $files[] = [
            'size' => $file->getSize(),
            'date' => $file->getCTime(),
            'name' => $file->getFilename(),
        ];
    }
}

usort($files, function($a, $b){
    if ($a['size'] > $b['size']){
        return -1;
    } else if ($a['size'] < $b['size']) {
        return 1;
    } 
    if ($a['date'] > $b['date']) {
        return 1;
    } else if ($a['date'] < $b['date']) {
        return -1;
    } 
    return 0;
});

foreach ($files as $file) {
    printf(
        "%s, %s, %.2f kb \n", 
        $file['name'], 
        date("d-m-Y", $file['date']), 
        $file['size'] / 1024
    );
}
