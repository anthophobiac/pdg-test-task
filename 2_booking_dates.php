<?php

// dummy data for test purposes
$dates = [
    '01.11.2019-05.11.2019', 
    '18.11.2019-02.12.2019',
    '07.11.2019-08.11.2019',
    '08.11.2019-10.11.2019',
]; 

if ($argc < 3) {
    printf("Not enough arguments passed.\nUsage: php dates.php MONTH INTERVAL\n");
    exit(1);
}

$requestedMonth = (int)$argv[1];
$requestedInterval = (int)$argv[2];

if ($requestedMonth > 12 || $requestedMonth < 1) {
    printf("Invalid MONTH argument, valid values are from 1 to 12.\n");
    exit(1);
}

// parsing $dates and preparing the data for further comparison
$booked = array_map(function($bookedInterval) {
    list($begin, $end) = explode('-', $bookedInterval);
    return [
        DateTime::createFromFormat('d.m.Y', $begin)->format('Ymd'),
        DateTime::createFromFormat('d.m.Y', $end)->format('Ymd'),
    ];
}, $dates);

$now = new DateTime();
$year = (int)$now->format('Y');
$currentMonth = (int)$now->format('m');
$startDay = ($requestedMonth == $currentMonth) ? (int)$now->format('d') : 1;

if ($currentMonth > $requestedMonth) {
    $year++;
}

$currentIntervalBegin = new DateTime();
$currentIntervalBegin->setDate($year, $requestedMonth, $startDay);
$lastDayOfMonth = (int)$currentIntervalBegin->format('t');

for ($i = $startDay; $i <= $lastDayOfMonth; $i++) {
    $endDay = clone $currentIntervalBegin;
    $endDay->modify("+{$requestedInterval} days");
    $available = true;

    // formatting begin and end of the interval in order to compare only the date
    $currentBegin = $currentIntervalBegin->format('Ymd');
    $currentEnd = $endDay->format('Ymd');

    foreach ($booked as $booking) {
        list($bookingBegin, $bookingEnd) = $booking;
        if (
            ($currentBegin >= $bookingBegin && $currentBegin < $bookingEnd) || 
            ($currentEnd > $bookingBegin && $currentEnd <= $bookingEnd)
            ) {
                $available = false;
                break;
        }
    }

    if ($available) {
        printf("%s - %s\n", $currentIntervalBegin->format('d.m.Y'), $endDay->format('d.m.Y'));
    }
    $currentIntervalBegin->modify('+1 day');
}
